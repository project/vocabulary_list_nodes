Description
-----------

Provides nodes listing by vocabulary with standart pager.
Lists are available at:
 * http://www.example.com/vocabulary/list/1;
 * http://www.example.com/vocabulary/list/1+2+3 and so on;
 * http://www.example.com/vocabulary/list/1,2,3 and so on;
 * http://www.example.com/vocabulary/list/1 2 3 and so on;
where 1, 2 and 3 are vocabulary IDs.

Installation
------------

1) Copy vocabulary_list.module to your modules/ directory.

2) Visit the "admin/build/modules" page to enable the module.

3) Navigate to http://www.example.com/vocabulary/list/1+2+3 to view your vocabularys
